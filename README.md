# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is just a test to the WebRadar employment process.

### How do I get set up? ###

This project uses PHP 5.6 with MySql 5.7. I downloaded the EasyPHP Devserver 16.1.1 from http://www.easyphp.org/ to make easier the enviroment configuration and setup.

After that, just move the radar folder to your www folder, run the scripts and you are ready to go!

### Contribution guidelines ###
Please, don't. This project is closed and is just to assert my skills.