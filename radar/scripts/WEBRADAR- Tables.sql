SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `rnc_log` (
  `ID` varchar(10) NOT NULL,
  `PARAM1` varchar(10) NOT NULL,
  `VALOR1` varchar(10) NOT NULL,
  `PARAM2` varchar(10) NOT NULL,
  `VALOR2` varchar(10) NOT NULL,
  `PARAM3` varchar(10) NOT NULL,
  `VALOR3` varchar(10) NOT NULL,
  `PARAM4` varchar(10) NOT NULL,
  `VALOR4` varchar(10) NOT NULL,
  `SOURCE` varchar(16) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Tabela de logs da Tellus do Canadá';

ALTER TABLE `rnc_log`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`);