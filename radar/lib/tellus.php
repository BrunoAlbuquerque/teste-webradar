<?php
	class Tellus {
		var $path;
		private $dbConnect;
		
		public function __construct (DBConnect $dbConnect) {
			$this->path = "ftp";
			$this->dbConnect = $dbConnect;
		}
		
		public function readFTP () {
			print "<p><b>Iniciando Leitura dos Logs</b></p>";
			$readLogs = $this->dbConnect->logList(); //Pegando Lista de Logs Ja Gravados
			foreach (glob($this->path."/*.log") as $filepath) { //Lendo Todos os Arquivos .log do Caminho
				$filename = substr(substr($filepath, 0, -4), 4); //Tratando Nome do Arquivo
				if (!in_array($filename, $readLogs)) { //Significa que o Log nao foi Gravado
					$this->readLog($filepath, $filename);
					print "Inserindo $filename... <br />"; //Num Ambiente Real, Fazer Essa Listagem Seria Desnecessaria
				} else {
					print "$filename ja foi inserido. <br />";
				}
			}
			print "<p><b>Fim da Leitura dos Logs</b></p>";
		}
		
		private function readLog($filepath, $filename) {
			$pattern  = '/^RNCNN_(\d){10}~/'; //Regex do Formato do Nome do Log
			
			//Verificando se Arquivo Nao Esta Vazio, Pode Ser Lido e Nome Confere
			if (!filesize($filepath)) {
				print "Arquivo de nome $filename.log vazio.";
			} else if (!is_readable($filepath)) {
				print "Arquivo de nome $filename.log nao pode ser lido.";
			} else if (preg_match($pattern, $filename)) {
				print "Arquivo de nome $filename.log nao esta com nome formatado corretamente.";
			} else { //Tudo OK
				$handle = fopen($filepath, "r"); //Abrindo o Log
				if ($handle) {
					while (($line = fgets($handle)) !== false) { //Lendo Linha a Linha
						$this->writeLine($line, $filename);
					}
					fclose($handle);
				} else {
					print "Erro ao abrir o arquivo $filename.log.";
				} 
			}
		}
		
		//Listando a Soma de Todos os Valores de PARAM1 por Hora
		public function readLogAggregation() { 
			print "<p><b>Listando Agregacao de Valores de PARAM1 por Hora</b></p>";
			foreach ($this->dbConnect->logAggregation() as &$value) {
				print "Data e Hora:$value[0], Total: $value[1] <br />";
			} 
		}
		
		private function writeLine($line, $filename) {
			$values = explode("|", $line);
			$id 	= $values[0]; 
			$param1 = $values[1];
			$valor1 = $values[2];
			$param2 = $values[3];
			$valor2 = $values[4];
			$param3 = $values[5];
			$valor3 = $values[6];
			$param4 = $values[7];
			$valor4 = $values[8];
			$source = $filename;
			
			$this->dbConnect->insertLog($id, $param1, $valor1, $param2, $valor2, $param3, $valor3, $param4, $valor4, $source);
		}
	}
?>