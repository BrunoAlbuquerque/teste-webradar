<?php
	/*
		Por Questões de Segurança, os Dados de Conexão Deveriam ser Armazenados no httpd.conf da Seguinte Maneira:
	
		php_value mysql.default.user      root
		php_value mysql.default.password  null
		php_value mysql.default.database  webradar
		php_value mysql.default.host      127.0.0.1
	*/
	
	class DBConnect {
		var $link;
		var $password;
		var $username;
		var $database;
		var $hostname;
		
		function __construct () {
			$this->password = "C8r29xSdRpTl7HmB";
			$this->username = "webradar";
			$this->database = "webradar";
			$this->hostname = "127.0.0.1";
		}
		
		function openCon () {
			$this->link = new mysqli($this->hostname, $this->username, $this->password, $this->database);
			if ($this->link->connect_error) {
				die('Erro de Conexão ('.$this->link->connect_errno.')'.$this->link->connect_error);
			}
		}
		
		function insertLog($id, $param1, $valor1, $param2, $valor2, $param3, $valor3, $param4, $valor4, $source) {
			$stmt =  $this->link->stmt_init();
			if ($stmt->prepare('INSERT INTO `rnc_log` 
								(`ID`, `PARAM1`, `VALOR1`, `PARAM2`, `VALOR2`, `PARAM3`, `VALOR3`, `PARAM4`, `VALOR4`, `SOURCE`) 
								VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)')) {
				$stmt->bind_param("ssssssssss", $id, $param1, $valor1, $param2, $valor2, $param3, $valor3, $param4, $valor4, $source);

				try {
					$stmt->execute();
				} catch (Exception $e) {
					 print 'Foi encontrado o seguinte problema: '.$e->getMessage().'\n';
				} finally {
					$stmt->close();
				}
			}
		}
		
		function logList() {
			$rows = array();
			$query  = "SELECT DISTINCT source FROM `rnc_log`";
			
			try {
				$result = $this->link->query($query);
				
				while($row = $result->fetch_row()) {
					$rows[] = end($row);
				}
				
			} catch (Exception $e) {
				 print 'Foi encontrado o seguinte problema: '.$e->getMessage().'\n';
			} finally {
				$result->free();
				return $rows;
			}
		}
		
		function logAggregation() {
			$rows = array();
			$query  = "SELECT * FROM `log_aggregation`";
			
			try {
				$result = $this->link->query($query);
				
				while($row = $result->fetch_row()) {
					$rows[] = $row;
				}
				
			} catch (Exception $e) {
				 print 'Foi encontrado o seguinte problema: '.$e->getMessage().'\n';
			} finally {
				$result->free();
				return $rows;
			}
		}
	}
?>