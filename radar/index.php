<!DOCTYPE html>
<html>
	<head>
		<title>Teste de BackEndDev WebRadar</title>
		<meta charset="utf-8" />
	</head>
	<body>
	<h1>PHP 5.6 + MySQL 5.7</h1>
	<?php
		/*
			Normalmente eu utilizaria a função ftp_nlist para ler um resultado de um ftp_connect com um 
			ftp_login, mas não tenho acesso a um servidor no momento, então estou lendo uma pasta local.
		*/

		require_once("lib/connect.php");
		require_once("lib/tellus.php");
		
		$con = new DBConnect();
		$con->openCon();
		
		$log = new Tellus($con);
		$log->readFTP();
		$log->readLogAggregation();
	?>
	</body>
</html>